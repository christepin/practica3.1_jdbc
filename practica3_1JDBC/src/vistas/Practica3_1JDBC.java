
package vistas;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Conexion;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.util.ArrayList;

/**
 *
 * @author chris
 */
public class Practica3_1JDBC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
       
       
       XStream xstream = new XStream(new DomDriver());
       
       Conexion c = new Conexion("curso", "root", ""); //tiene el password dentro el fichero txt mandado por Teams
       String query = "select * from profesor;";
       
       ResultSet rs =  c.consulta(query);
       
       String str = xstream.toXML(c);
       
        try {
            while( rs.next() ){
                System.out.println("[ ID: " +  rs.getString(1) +" ] " + " [ Nombre: " + rs.getString(2) +" ] " + " [ Apellido: " + rs.getString(3)+" ] " + " [ Email: " + rs.getString(4)+ " ]" );
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(Practica3_1JDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        c.closeConexion();
        
        System.out.println("\n\n=======XML CONNECCION========");
        System.out.println(str);
               
    }
    
}
