/**------Insert estudiante------**/

INSERT INTO estudiante
VALUES (100895,"Willy Christian","Fokoua Boyoum");

INSERT INTO estudiante
VALUES (125536,"junior jule","March wellidestudiante"); **/

INSERT INTO estudiante
VALUES (445926,"Patt Chris","Remilton");

INSERT INTO estudiante
VALUES (631845,"Eliane Michelle","Beans");

/**-------Insert Profesor----------------**/

INSERT INTO profesor
VALUES (852695,"Leon Skott","Kennedy","leonK@curso.es");

INSERT INTO profesor
VALUES (488125,"Chris Wilfrid","Sanz","chrisW@curso.es");

INSERT INTO profesor
VALUES (999662,"Ashley Dream","Drewar","ashleyD@curso.es");

INSERT INTO profesor
VALUES (784556,"Ada Elise","Wong","adaE@curso.es");

/**-------Insert clase---------------**/

INSERT INTO clase
VALUES (1001,"Acceso a Datos","AD");

INSERT INTO clase
VALUES (1002,"Lenguaje de Marca","LM");

INSERT INTO clase
VALUES (1003,"Programación","PRO");

INSERT INTO clase
VALUES (1004,"Inglés","ING");

/**------------Insert clase_estudiante--------**/


INSERT INTO clase_estudiante
VALUES (1,100895,1001);

INSERT INTO clase_estudiante
VALUES (2,100895,1002);

INSERT INTO clase_estudiante
VALUES (3,100895,1003);

INSERT INTO clase_estudiante
VALUES (4,100895,1004);


INSERT INTO clase_estudiante
VALUES (5,631845,1001);

INSERT INTO clase_estudiante
VALUES (6,631845,1002);

INSERT INTO clase_estudiante
VALUES (7,631845,1003);

INSERT INTO clase_estudiante
VALUES (8,631845,1004);


INSERT INTO clase_estudiante
VALUES (9,445926,1001);

INSERT INTO clase_estudiante
VALUES (10,445926,1002);

INSERT INTO clase_estudiante
VALUES (11,125536,1003);

INSERT INTO clase_estudiante
VALUES (12,125536,1004);


/**------------Insert clase_profesor--------**/

INSERT INTO clase_profesor
VALUES (1,852695,1001);

INSERT INTO clase_profesor
VALUES (2,488125,1002);

INSERT INTO clase_profesor
VALUES (3,999662,1003);

INSERT INTO clase_profesor
VALUES (4,784556,1004);