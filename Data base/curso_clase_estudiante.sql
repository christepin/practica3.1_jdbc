-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: curso
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clase_estudiante`
--

DROP TABLE IF EXISTS `clase_estudiante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clase_estudiante` (
  `idclase_estudiante` int NOT NULL AUTO_INCREMENT,
  `idestudiante_fk` int unsigned NOT NULL,
  `idclase_fk` int unsigned NOT NULL,
  PRIMARY KEY (`idclase_estudiante`),
  KEY `idestudianteFk_idx` (`idestudiante_fk`),
  KEY `idclaseFk_idx` (`idclase_fk`),
  CONSTRAINT `idclaseFk2` FOREIGN KEY (`idclase_fk`) REFERENCES `clase` (`idclase`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `idestudianteFk` FOREIGN KEY (`idestudiante_fk`) REFERENCES `estudiante` (`idestudiante`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clase_estudiante`
--

LOCK TABLES `clase_estudiante` WRITE;
/*!40000 ALTER TABLE `clase_estudiante` DISABLE KEYS */;
INSERT INTO `clase_estudiante` VALUES (1,100895,1001),(2,100895,1002),(3,100895,1003),(4,100895,1004),(5,631845,1001),(6,631845,1002),(7,631845,1003),(8,631845,1004),(9,445926,1001),(10,445926,1002),(11,125536,1003),(12,125536,1004);
/*!40000 ALTER TABLE `clase_estudiante` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-12 20:38:30
